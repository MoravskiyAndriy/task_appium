package com.moravskiyandriy.decorator;

import com.moravskiyandriy.webelement.BasicAndroidElement;
import com.moravskiyandriy.webelement.implementations.Button;
import com.moravskiyandriy.webelement.implementations.CheckBox;
import com.moravskiyandriy.webelement.implementations.InputTextField;
import com.moravskiyandriy.webelement.implementations.Label;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.pagefactory.DefaultFieldDecorator;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

import java.lang.reflect.*;
import java.util.List;
import java.util.Objects;

public class CustomFieldDecorator extends DefaultFieldDecorator {
    public CustomFieldDecorator(ElementLocatorFactory factory) {
        super(factory);
    }

    @Override
    public Object decorate(ClassLoader loader, Field field) {
        ElementLocator locator = factory.createLocator(field);

        if (Button.class.isAssignableFrom(field.getType())) {
            return new Button(proxyForLocator(loader, locator));
        } else if (InputTextField.class.isAssignableFrom(field.getType())) {
            return new InputTextField(proxyForLocator(loader, locator));
        } else if (CheckBox.class.isAssignableFrom(field.getType())) {
            return new CheckBox(proxyForLocator(loader, locator));
        } else if (Label.class.isAssignableFrom(field.getType())) {
            return new Label(proxyForLocator(loader, locator));
        } else if (List.class.isAssignableFrom(field.getType())) {
            Class<BasicAndroidElement> decoratableClass = decoratableClass(field);
            return createList(loader, locator, decoratableClass);
        } else if (BasicAndroidElement.class.isAssignableFrom(field.getType())) {
            return new BasicAndroidElement(proxyForLocator(loader, locator));
        } else {
            return super.decorate(loader, field);
        }
    }

    @SuppressWarnings("unchecked")
    private List<BasicAndroidElement> createList(ClassLoader loader, ElementLocator locator, Class<BasicAndroidElement> clazz) {
        InvocationHandler handler = new CustomElementListHandler(locator, clazz);
        return (List<BasicAndroidElement>) Proxy.newProxyInstance(
                loader, new Class[]{List.class}, handler);
    }

    @SuppressWarnings("unchecked")
    private Class<BasicAndroidElement> decoratableClass(Field field) {
        Class<?> clazz = field.getType();
        if (List.class.isAssignableFrom(clazz)) {
            if (Objects.isNull(field.getAnnotation(FindBy.class)) &&
                    Objects.isNull(field.getAnnotation(FindBys.class))) {
                return null;
            }
            Type genericType = field.getGenericType();
            if (!(genericType instanceof ParameterizedType)) {
                return null;
            }
            clazz = (Class<?>) ((ParameterizedType) genericType).
                    getActualTypeArguments()[0];
        }
        if (BasicAndroidElement.class.isAssignableFrom(clazz)) {
            return (Class<BasicAndroidElement>) clazz;
        } else {
            return null;
        }
    }
}

