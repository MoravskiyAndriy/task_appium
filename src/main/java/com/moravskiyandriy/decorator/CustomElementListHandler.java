package com.moravskiyandriy.decorator;

import com.moravskiyandriy.webelement.BasicAndroidElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class CustomElementListHandler implements InvocationHandler {
    private final ElementLocator locator;
    private final Class<BasicAndroidElement> clazz;

    CustomElementListHandler(ElementLocator locator, Class<BasicAndroidElement> clazz) {
        this.locator = locator;
        this.clazz = clazz;
    }

    public Object invoke(Object object, Method method, Object[] objects) throws Throwable {
        List<WebElement> elements = locator.findElements();
        List<BasicAndroidElement> customs = new ArrayList<>();
        for (WebElement element : elements) {
            customs.add(createInstance(clazz, element));
        }
        try {
            return method.invoke(customs, objects);
        } catch (InvocationTargetException e) {
            throw e.getCause();
        }
    }

    private static BasicAndroidElement createInstance(Class<BasicAndroidElement> clazz, WebElement element) {
        try {
            return clazz.getConstructor(WebElement.class).
                    newInstance(element);
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            throw new AssertionError("WebElement can't be represented as " + clazz);
        }
    }
}
