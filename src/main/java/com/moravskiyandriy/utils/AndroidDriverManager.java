package com.moravskiyandriy.utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class AndroidDriverManager {
    private static final Logger LOGGER = LogManager.getLogger(AndroidDriverManager.class);
    private static final String DEFAULT_DEVICE_UDID = "11111111";
    private static final String DEVICE_UDID = Optional.ofNullable(getProperties()
            .getProperty("DEVICE_UDID"))
            .orElse(DEFAULT_DEVICE_UDID);
    private static final ThreadLocal<WebDriver> driver_pool = new ThreadLocal<>();

    private AndroidDriverManager() {
    }

    public static WebDriver getDriver() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "7.1.2");
        capabilities.setCapability("deviceName", "Redmi 4A");
        capabilities.setCapability("udid", DEVICE_UDID);
        capabilities.setCapability("appPackage", "com.google.android.gm");
        capabilities.setCapability("appActivity", "com.google.android.gm.GmailActivity");

        if (Objects.isNull(driver_pool.get())) {
            AppiumDriver driver = null;
            try {
                driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
            } catch (MalformedURLException e) {
                LOGGER.info("Dab URL");
                e.printStackTrace();
            }
            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
            driver_pool.set(driver);
        }
        return driver_pool.get();
    }

    public static void setDriver(AppiumDriver driver) {
        driver_pool.set(driver);
    }

    public static void quitDriver() {
        if (Objects.nonNull(driver_pool.get())) {
            driver_pool.get().quit();
        }
    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = AndroidDriverManager.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            LOGGER.warn("NumberFormatException found.");
        } catch (IOException ex) {
            LOGGER.warn("IOException found.");
        }
        return prop;
    }
}
