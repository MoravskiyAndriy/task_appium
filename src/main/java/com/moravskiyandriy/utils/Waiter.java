package com.moravskiyandriy.utils;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waiter {
    public static final int SMALL_WAIT = 10;
    public static final int MEDIUM_WAIT = 30;
    public static final int LONG_WAIT = 60;

    private Waiter() {
    }

    public static WebElement waitForVisibilityOfElement(WebElement element, int time) {
        return (new WebDriverWait(AndroidDriverManager.getDriver(), time)).ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.visibilityOf(element));
    }

    public static WebElement waitForElementToBeClickable(WebElement element, int time) {
        return (new WebDriverWait(AndroidDriverManager.getDriver(), time)).ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.elementToBeClickable(element));
    }
}
