package com.moravskiyandriy.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Utils {
    private static final Logger LOGGER = LogManager.getLogger(Utils.class);

    private Utils() {
    }

    public static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = Utils.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            LOGGER.warn("NumberFormatException found.");
        } catch (IOException ex) {
            LOGGER.warn("IOException found.");
        }
        return prop;
    }
}
