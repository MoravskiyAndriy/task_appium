package com.moravskiyandriy.dataparcer;

import com.opencsv.CSVReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CSVDataParser {
    private CSVDataParser() {
    }

    public static Iterator<Object[]> readFile(String path) {
        String csvFile = path;
        List<Object[]> data = new ArrayList<>();
        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader(csvFile));
            String[] line;
            while ((line = reader.readNext()) != null) {
                data.add(new String[]{line[0], line[1]});
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data.stream().iterator();
    }
}
