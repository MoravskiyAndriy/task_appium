package com.moravskiyandriy.fortest;

import com.moravskiyandriy.dataparcer.CSVDataParser;
import com.moravskiyandriy.utils.AndroidDriverManager;
import com.moravskiyandriy.utils.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;

import java.util.Iterator;
import java.util.Optional;

public class BaseTest {
    private static final Logger LOGGER = LogManager.getLogger(BaseTest.class);
    private static final String DATAFILE_PATH = "./src/main/resources/UserData.csv";
    private static final int DEFAULT_QUANTITY_OF_LETTERS_TO_DELETE = 3;
    protected static final int FINAL_QUANTITY_OF_LETTERS_TO_DELETE = Optional.ofNullable(Utils.getProperties()
            .getProperty("QUANTITY_OF_LETTERS_TO_DELETE"))
            .map(Integer::valueOf)
            .orElse(DEFAULT_QUANTITY_OF_LETTERS_TO_DELETE);

    @DataProvider()
    protected Iterator<Object[]> users() {
        return CSVDataParser.readFile(DATAFILE_PATH);
    }

    public WebDriver getDriver() {
        return AndroidDriverManager.getDriver();
    }

    private void onTestFailure(ITestResult iTestResult) {
        LOGGER.info("===TEST FAILED===");
    }

    private void afterActions(ITestResult iTestResult) {
        if (!iTestResult.isSuccess()) {
            onTestFailure(iTestResult);
        }
    }

    @AfterMethod
    protected void quitDriver(ITestResult iTestResult) {
        LOGGER.info("Quit driver");
        afterActions(iTestResult);
        AndroidDriverManager.quitDriver();
        AndroidDriverManager.setDriver(null);
    }
}
