package com.moravskiyandriy.pageobjects.implementations;

import com.moravskiyandriy.pageobjects.AbstractPageObject;
import com.moravskiyandriy.utils.listeners.TestListener;
import com.moravskiyandriy.webelement.implementations.Button;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.Listeners;

@Listeners({TestListener.class})
public class ReverseActionPopUp extends AbstractPageObject {
    private static final Logger LOGGER = LogManager.getLogger(ReverseActionPopUp.class);

    @FindBy(id = "action_text")
    private Button undoButton;

    @Step("Clicking 'undo' button.")
    public void clickUndoButton() {
        LOGGER.info("clicking Undo Button");
        undoButton.tap();
    }
}
