package com.moravskiyandriy.pageobjects.implementations;

import com.moravskiyandriy.pageobjects.AbstractPageObject;
import com.moravskiyandriy.utils.listeners.TestListener;
import com.moravskiyandriy.webelement.implementations.Button;
import com.moravskiyandriy.webelement.implementations.InputTextField;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.Listeners;

@Listeners({TestListener.class})
public class AuthenticationPage extends AbstractPageObject {
    private static final Logger LOGGER = LogManager.getLogger(AuthenticationPage.class);
    private static int WAITING_FOR_REFRESH = 7000;
    @FindBy(id = "welcome_tour_got_it")
    private Button gotItButton;
    @FindBy(id = "setup_addresses_add_another")
    private Button addAnotherAccountButton;
    @FindBy(xpath = "//android.widget.TextView[contains(@text,'Google')]")
    private Button chooseGoogleButton;
    @FindBy(xpath = "//android.widget.EditText")
    private InputTextField loginField;
    @FindBy(xpath = "//android.widget.EditText")
    private InputTextField passwordField;
    @FindBy(xpath = "//android.widget.Button[contains(@text,'Далі')]")
    private Button nextButton;
    @FindBy(xpath = "//android.widget.Button[contains(@text,'Прийняти')]")
    private Button acceptConditionsButton;
    @FindBy(xpath = "//android.widget.Switch")
    private Button saveIntoCloudCheckBox;
    @FindBy(xpath = "//android.widget.Button[contains(@text,'ПРИЙНЯТИ')]")
    private Button acceptFinalTermsButton;
    @FindBy(id = "action_done")
    private Button enterMailBoxButton;

    @Step("Going to gmail page.")
    public void goToGmailPage() {
        LOGGER.info("going to Gmail Page");
        gotItButton.tap();
    }

    @Step("Going to gmail page.")
    public void extendedGoToGmailPage() {
        LOGGER.info("going to Gmail Page");
        gotItButton.tap();
        addAnotherAccountButton.tap();
        chooseGoogleButton.tap();
    }

    @Step("Filling login field with {0}.")
    public void fillLoginField(String login) {
        LOGGER.info("filling Login Field");
        loginField.tap();
        loginField.sendKeys(login);
    }

    @Step("Pressing 'Next' Button.")
    public void pressingNextAfterLogin() {
        LOGGER.info("pressing 'Next' Button.");
        nextButton.tap();
    }

    @Step("Filling password field with {0}.")
    public void fillPasswordField(String password) {
        LOGGER.info("filling Password Field");
        passwordField.tap();
        passwordField.sendKeys(password);
    }

    @Step("Pressing 'Next' Button.")
    public void pressingNextAfterPassword() {
        LOGGER.info("pressing 'Next' Button.");
        nextButton.tap();
    }

    @Step("Accepting conditions")
    public void acceptingConditions() {
        LOGGER.info("Accepting conditions");
        scrollDown();
        acceptConditionsButton.tap();
        saveIntoCloudCheckBox.tap();
        scrollDown();
        acceptFinalTermsButton.tap();
        try {
            Thread.sleep(WAITING_FOR_REFRESH);
        } catch (InterruptedException e) {
            LOGGER.info("sleep interrupted");
        }
    }

    @Step("Entering mailBox")
    public void enterMailBox() {
        LOGGER.info("Entering mailBox");
        enterMailBoxButton.tap();
    }
}
