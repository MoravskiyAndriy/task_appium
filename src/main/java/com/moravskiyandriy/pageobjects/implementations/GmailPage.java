package com.moravskiyandriy.pageobjects.implementations;

import com.moravskiyandriy.pageobjects.AbstractPageObject;
import com.moravskiyandriy.utils.listeners.TestListener;
import com.moravskiyandriy.webelement.implementations.Button;
import com.moravskiyandriy.webelement.implementations.Label;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.Listeners;

import java.util.List;

@Listeners({TestListener.class})
public class GmailPage extends AbstractPageObject {
    private static final Logger LOGGER = LogManager.getLogger(GmailPage.class);

    @FindBy(xpath = "//android.widget.ImageButton[@content-desc='Висунути панель навігації']")
    private Button navigationPanelButton;
    @FindBy(xpath = "(//android.widget.TextView[@resource-id='com.google.android.gm:id/unread'])[1]")
    private Label letterQuantityLabel;
    @FindBy(xpath = "//android.widget.ImageView[@content-desc='Закрити пораду']")
    private Button dismissButton;
    @FindBy(id = "search")
    private Button searchButton;
    @FindBy(xpath = "//android.view.View[@index=0]")
    private List<Button> incomingLetters;
    @FindBy(xpath = "//android.widget.TextView[@content-desc='Видалити']")
    private Button deleteButton;

    @Step("Clicking navigation Panel Button.")
    private void clicknavigationPanelButton() {
        LOGGER.info("Clicking navigation Panel Button.");
        navigationPanelButton.tap();
    }

    @Step("Checking out letters for deletion.")
    public void checkLettersForDeletion(int quantity) {
        LOGGER.info("checking Letters for deletion");
        for (int i = 1; i <= quantity; i++) {
            incomingLetters.get(i).longTap(driver);
        }
    }

    @Step("Clicking 'delete' button.")
    public void clickDeleteButton() {
        LOGGER.info("clicking 'delete' Button");
        deleteButton.tap();
    }

    @Step("Counting letters in 'incoming'.")
    public int countIncomingLetters() {
        LOGGER.info("getting letters' quantity");
        clicknavigationPanelButton();
        String labelText = letterQuantityLabel.getText().trim();
        searchButton.tap();
        return Integer.parseInt(labelText);
    }

    @Step("Clicking 'dismiss message' button.")
    public void clickDismissButton() {
        LOGGER.info("clicking dismiss message Button");
        dismissButton.tap();
    }
}
