package com.moravskiyandriy.pageobjects;

import com.moravskiyandriy.decorator.CustomFieldDecorator;
import com.moravskiyandriy.utils.AndroidDriverManager;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class AbstractPageObject {
    private static final Logger LOGGER = LogManager.getLogger(AbstractPageObject.class);
    protected WebDriver driver;

    protected AbstractPageObject() {
        driver = AndroidDriverManager.getDriver();
        PageFactory.initElements(new CustomFieldDecorator(
                new DefaultElementLocatorFactory(AndroidDriverManager.getDriver())), this);
    }

    protected void scrollDown() {
        int pressX = driver.manage().window().getSize().width / 2;
        int bottomY = driver.manage().window().getSize().height * 4 / 5;
        int topY = driver.manage().window().getSize().height / 8;
        scroll(pressX, bottomY, pressX, topY);
    }

    private void scroll(int fromX, int fromY, int toX, int toY) {
        TouchAction touchAction = new TouchAction((AndroidDriver) driver);
        touchAction.longPress(PointOption.point(fromX, fromY)).moveTo(PointOption.point(toX, toY)).release().perform();
    }
}
