package com.moravskiyandriy.webelement.implementations;

import com.moravskiyandriy.utils.Waiter;
import com.moravskiyandriy.webelement.BasicAndroidElement;
import org.openqa.selenium.WebElement;

public class Label extends BasicAndroidElement {
    public Label(WebElement androidElement) {
        super(androidElement);
    }

    public String getText() {
        Waiter.waitForVisibilityOfElement(androidElement, Waiter.MEDIUM_WAIT);
        return androidElement.getText();
    }

    public boolean isDisplayed() {
        return androidElement.isDisplayed();
    }
}
