package com.moravskiyandriy.webelement.implementations;

import com.moravskiyandriy.utils.Waiter;
import com.moravskiyandriy.webelement.BasicAndroidElement;
import org.openqa.selenium.WebElement;

public class CheckBox extends BasicAndroidElement {
    public CheckBox(WebElement androidElement) {
        super(androidElement);
    }

    public void setCheckBoxSelected() {
        Waiter.waitForElementToBeClickable(androidElement, Waiter.LONG_WAIT);
        if (!androidElement.isSelected()) {
            androidElement.click();
        }
    }

    public boolean isSelected() {
        Waiter.waitForElementToBeClickable(androidElement, Waiter.SMALL_WAIT);
        return androidElement.isSelected();
    }
}
