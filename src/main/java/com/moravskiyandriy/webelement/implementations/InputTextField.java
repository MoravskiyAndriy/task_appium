package com.moravskiyandriy.webelement.implementations;

import com.moravskiyandriy.utils.Waiter;
import com.moravskiyandriy.webelement.BasicAndroidElement;
import org.openqa.selenium.WebElement;

public class InputTextField extends BasicAndroidElement {
    public InputTextField(WebElement androidElement) {
        super(androidElement);
    }

    public void sendKeys(CharSequence... charSequences) {
        Waiter.waitForVisibilityOfElement(androidElement, Waiter.LONG_WAIT);
        androidElement.clear();
        androidElement.sendKeys(charSequences);
    }

    public String getText() {
        Waiter.waitForVisibilityOfElement(androidElement, Waiter.SMALL_WAIT);
        return androidElement.getText();
    }

    public void tap() {
        Waiter.waitForVisibilityOfElement(androidElement, Waiter.MEDIUM_WAIT);
        androidElement.click();
    }
}
