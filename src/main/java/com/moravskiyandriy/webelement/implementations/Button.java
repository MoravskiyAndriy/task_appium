package com.moravskiyandriy.webelement.implementations;

import com.moravskiyandriy.utils.AndroidDriverManager;
import com.moravskiyandriy.utils.Waiter;
import com.moravskiyandriy.webelement.BasicAndroidElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Button extends BasicAndroidElement {
    private static final int COORDINATES_CENTRALIZER = 100;

    public Button(WebElement androidElement) {
        super(androidElement);
    }

    public void tap() {
        Waiter.waitForVisibilityOfElement(androidElement, Waiter.LONG_WAIT);
        androidElement.click();
    }

    public void doubleTap() {
        Waiter.waitForVisibilityOfElement(androidElement, Waiter.SMALL_WAIT);
        new Actions(AndroidDriverManager.getDriver()).doubleClick(androidElement);
    }

    public void longTap(WebDriver driver) {
        new TouchAction((AndroidDriver) driver).
                longPress(PointOption.point(androidElement.getLocation().x + COORDINATES_CENTRALIZER,
                        androidElement.getLocation().y))
                .release().perform();
    }
}
