package com.moravskiyandriy.webelement;

import org.openqa.selenium.*;

import java.util.List;

public class BasicAndroidElement implements WebElement {
    protected WebElement androidElement;

    public BasicAndroidElement(WebElement androidElement) {
        this.androidElement = androidElement;
    }

    @Override
    public void click() {
        androidElement.click();
    }

    @Override
    public void submit() {
        androidElement.submit();
    }

    @Override
    public void sendKeys(CharSequence... charSequences) {
        androidElement.sendKeys(charSequences);
    }

    @Override
    public void clear() {
        androidElement.clear();
    }

    @Override
    public String getTagName() {
        return androidElement.getTagName();
    }

    @Override
    public String getAttribute(String s) {
        return androidElement.getAttribute(s);
    }

    @Override
    public boolean isSelected() {
        return androidElement.isSelected();
    }

    @Override
    public boolean isEnabled() {
        return androidElement.isEnabled();
    }

    @Override
    public String getText() {
        return androidElement.getText();
    }

    @Override
    public List<WebElement> findElements(By by) {
        return androidElement.findElements(by);
    }

    @Override
    public WebElement findElement(By by) {
        return androidElement.findElement(by);
    }

    @Override
    public boolean isDisplayed() {
        return androidElement.isDisplayed();
    }

    @Override
    public Point getLocation() {
        return androidElement.getLocation();
    }

    @Override
    public Dimension getSize() {
        return androidElement.getSize();
    }

    @Override
    public Rectangle getRect() {
        return androidElement.getRect();
    }

    @Override
    public String getCssValue(String s) {
        return androidElement.getCssValue(s);
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        return null;
    }
}
