package com.moravskiyandriy.businessobjects;

import com.moravskiyandriy.pageobjects.implementations.AuthenticationPage;

public class AuthenticationBO {
    private AuthenticationPage authenticationPage = new AuthenticationPage();

    public void loginIntoGmail(String login, String password) {
        authenticationPage.extendedGoToGmailPage();
        authenticationPage.fillLoginField(login);
        authenticationPage.pressingNextAfterLogin();
        authenticationPage.fillPasswordField(password);
        authenticationPage.pressingNextAfterPassword();
        authenticationPage.acceptingConditions();
        authenticationPage.enterMailBox();
    }

    public void enterLoggedUserMailBox() {
        authenticationPage.goToGmailPage();
        authenticationPage.enterMailBox();
    }
}
