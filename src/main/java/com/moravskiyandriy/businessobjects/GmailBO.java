package com.moravskiyandriy.businessobjects;

import com.moravskiyandriy.pageobjects.implementations.GmailPage;
import com.moravskiyandriy.pageobjects.implementations.ReverseActionPopUp;

import java.util.ArrayList;
import java.util.List;

public class GmailBO {
    private GmailPage gmailPage = new GmailPage();
    private ReverseActionPopUp reverseActionPopUp = new ReverseActionPopUp();
    private List<Integer> lettersQuantities = new ArrayList<>();

    public void goToIncomingAndCountLetters() {
        lettersQuantities.add(gmailPage.countIncomingLetters());
    }

    public void chooseLettersDeleteUndo(int quantity) {
        gmailPage.clickDismissButton();
        gmailPage.checkLettersForDeletion(quantity);
        gmailPage.clickDeleteButton();
        reverseActionPopUp.clickUndoButton();
    }

    public boolean BeforeLettersQuantityEqualsAfterLettersQuantity(){
        return lettersQuantities.get(0).equals(lettersQuantities.get(1));
    }
}
