package com.moravskiyandriy.androidtest;

import com.moravskiyandriy.businessobjects.AuthenticationBO;
import com.moravskiyandriy.businessobjects.GmailBO;
import com.moravskiyandriy.fortest.BaseTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class DeleteLettersAndReverseActionAndroidLoggedUserTest extends BaseTest {

    @Test
    public void DeleteLettersAndReverseAction() {
        AuthenticationBO authenticationBO = new AuthenticationBO();
        GmailBO gmailBO = new GmailBO();
        authenticationBO.enterLoggedUserMailBox();
        gmailBO.goToIncomingAndCountLetters();
        gmailBO.chooseLettersDeleteUndo(FINAL_QUANTITY_OF_LETTERS_TO_DELETE);
        gmailBO.goToIncomingAndCountLetters();
        assertTrue(gmailBO.BeforeLettersQuantityEqualsAfterLettersQuantity());
    }
}
