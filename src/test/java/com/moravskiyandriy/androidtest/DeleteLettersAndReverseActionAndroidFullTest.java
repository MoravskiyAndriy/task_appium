package com.moravskiyandriy.androidtest;

import com.moravskiyandriy.businessobjects.AuthenticationBO;
import com.moravskiyandriy.businessobjects.GmailBO;
import com.moravskiyandriy.fortest.BaseTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class DeleteLettersAndReverseActionAndroidFullTest extends BaseTest {

    @Test(dataProvider = "users")
    public void DeleteLettersAndReverseAction(String accountAddress, String accountPassword) {
        AuthenticationBO authenticationBO = new AuthenticationBO();
        GmailBO gmailBO = new GmailBO();
        authenticationBO.loginIntoGmail(accountAddress, accountPassword);
        gmailBO.goToIncomingAndCountLetters();
        gmailBO.chooseLettersDeleteUndo(FINAL_QUANTITY_OF_LETTERS_TO_DELETE);
        gmailBO.goToIncomingAndCountLetters();
        assertTrue(gmailBO.BeforeLettersQuantityEqualsAfterLettersQuantity());
    }
}
